const request = require('request-promise-native');
const fs = require('fs');
const path = require('path');
const config = require('../../config.json');

async function OpcodeHelper() {
  console.log('[opcode-helper] Opcode update started!');

  const url = 'https://raw.githubusercontent.com/caali-hackerman/tera-data/master/mappings.json';
  const mappings = JSON.parse(await request({url: url}));
  const protocol = mappings[config.region].version;
  const filename = `protocol.${protocol}.map`;

  let options = {
      uri: `https://bitbucket.org/terazone/opcode-helper/raw/HEAD/opcodes/${filename}`,
      simple: false
  };

  request(`https://bitbucket.org/terazone/opcode-helper/raw/HEAD/opcodes/${filename}`)
      .then(function (response) {
        fs.writeFileSync(path.join(__dirname, '../..', 'node_modules', 'tera-data', 'map', filename), response);
        console.log('[opcode-helper] Opcode update completed!');
      })
      .catch(function (err) {
        console.log('[opcode-helper] Opcode update failed!');
      });
}

module.exports = OpcodeHelper
